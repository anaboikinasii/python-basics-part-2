
n basics tasks

#### What is it for

Feel free to add solutions to your tasks here. This repository
is to store your code and receive feedback. 

#### How get code to your machine
1. Install git (if not installed yet).
2. Do: ```git clone https://anaboikinasii@bitbucket.org/anaboikinasii/python-basics.git```
   This will fetch the repository code to your machine and you will be able
   to contribute.

#### How to add your code to the repository
1. cd to the repository where you have cloned the code (see step 1
   from "How to get code to your machine section").
2. Run ```git branch```. If branch is not "master", then run:
   ```git checkout master``` and check ```git branch``` is showing master.
3. Run ```git pull```. This will update your local code to the latest version
   from server.
4. Now, you need to switch to another branch to add your code and not interfere
   with others. Do do it, run ```git checkout -b <your-branch-name>```.
5. Now you can write code.
6. After you're finished it's time to put your code to review. 
7. Add the files you want to give for review by running:
   ```git add <./path/to/file/inside/repo>``` or if you want to add
   everything you have written, run: ```git add -A```.
8. Now, you need
   to describe what you have done and "version" it. So, run 
   ```git commit -m "<Some message describing what you have done>"```
9. Now, push the changes to bitbucket by runing:
   ```git push --set-upstream origin <your-branch-name>```.
10. Check your changes were pushed on server and create a "pull request".
   Please, refer to bitbucket documentation on how to do that, or just
   contact @anaboikina for questions. 
   
#### Where to add your code
There are 5 folders inside the repo. Each one represents tasks given 
during each meeting of the training. Create a folder named with your git
username inside appropriate folder. You can find an example in each folder.
Please, also add docstrings to the file and follow the basic pep8/flake8
standard rules. It will be also put on review :) 


